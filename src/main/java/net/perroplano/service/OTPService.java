package net.perroplano.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * One Time Password Service Interface
 * <p>
 * Created by Krzysztof Osiecki <krzysztof.osiecki@icloud.com> on 2015-12-21.
 */
public interface OTPService {
    
    /**
     * Generates safe secret for user
     *
     * @return secret
     */
    String generateSecret();
    
    /**
     * Generates one time password/code for given secret - part of combined Time Based OTP
     *
     * @param secret secret
     * @return one time passowrd
     * @throws NoSuchAlgorithmException exception
     * @throws InvalidKeyException      exception
     */
    long getCurrentCode(String secret) throws NoSuchAlgorithmException, InvalidKeyException;
    
    /**
     * Verifies if given code is valid for given secret - part of combined Time Based OTP
     *
     * @param secret secret
     * @param code   code
     * @return true if valid
     */
    boolean verify(String secret, long code);
    
    /**
     * Generates one time password/code for given secret and serial - part of combined Time and Serial Based OTP
     *
     * @param secret secret
     * @param serial serial (ie. code no 4)
     * @return one time password
     * @throws NoSuchAlgorithmException exception
     * @throws InvalidKeyException      exception
     */
    long getCodeForSerial(String secret, int serial) throws NoSuchAlgorithmException, InvalidKeyException;
    
    /**
     * Verifies if given code is valid for given secret and serial - part of combined Time and Serial Based OTP
     *
     * @param secret secret
     * @param code   code
     * @param serial serial
     * @return true if valid
     */
    boolean verify(String secret, long code, int serial);
}
