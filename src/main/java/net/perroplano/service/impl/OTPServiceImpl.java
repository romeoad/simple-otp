package net.perroplano.service.impl;

import net.perroplano.service.OTPService;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 * <h2>Time-based, Serial-based and Serial- and Time-based One Time Password Implementation.</h2>
 * <p>
 * TOTP algorithm has two parameters: time slice size on which given OTP is valid and
 * max drift size allowed between separate clocks to process OTP as valid. Both parameters
 * are <b>runtime changeable</b> as they are stored as java preferences.</p>
 * <p>
 * Serial- and Time-based OTP has also time related parameters as in TOTP, but it generates verification
 * codes using daily serial (code number X for current day), and expires them as they are used.
 * This OTP properties are:
 * - expires after verification
 * - expires after max slice size &times; 2 seconds
 * - expires if another code is generated
 * </p>
 * <p>
 * Path to preferences is:  /net/perroplano/service<br/>
 * slice size parameter is given in seconds<br/>
 * max drift size parameter is given in number of slices</p>
 * <p>
 * ie: slice = 60 [s] and max drift = 2 [slices] means that generated OTP will be
 * valid for 300s in very extreme case (shared secret alien token generator with
 * skewed clock) or 180s in case of single point of OTP generation and validation.</p>
 * <p>
 * Recommended values for web usage and single generation/validation point are:<br/>
 * time slice size 120 [s] (2 mins)<br/>
 * max drift size 1 (max 6 mins OTP live time effectively)</p>
 * <p>
 * OTP is guaranteed to be valid for slice size seconds.</p>
 * <small>
 * Created by Krzysztof Osiecki <krzysztof.osiecki@icloud.com> on 2015-11-24.</small>
 * <small>
 * Updated with serial and serial + time based code verification
 * by Krzysztof Osiecki <krzysztof.osiecki@icloud.com> on 2016-04-14.
 * </small>
 */
public class OTPServiceImpl implements OTPService {
    
    private final static Logger LOGGER = Logger.getLogger(OTPServiceImpl.class.getName());
    /**
     * Preference name for time slice duration for TOTP being valid
     */
    public static final String SLICE_SIZE = "totp-time-slice-size-in-seconds";
    /**
     * Preference name for how may time slices drift is allowed for TOTP being valid,
     * ie: code is valid for -DRIFT_UNITS slices, current slice and +DRIFT_UNITS slices
     */
    public static final String MAX_DRIFT = "totp-allowed-time-drift-in-slices";
    public static final int DEFAULT_SLICE_SIZE = 120;
    public static final int DEFAULT_MAX_DRIFT = 1;
    private Preferences prefs = Preferences.userNodeForPackage(OTPService.class);
    
    protected void setupPreferences() {
        int sliceSize = prefs.getInt(SLICE_SIZE, 0);
        int maxDrift = prefs.getInt(MAX_DRIFT, 0);
        // 30 second slice is possible though not recommended
        // otherwise reset to defaults: 120s +/- 1 slice
        if (sliceSize < 29 && maxDrift < 1) {
            prefs.putInt(SLICE_SIZE, DEFAULT_SLICE_SIZE);
            prefs.putInt(MAX_DRIFT, DEFAULT_MAX_DRIFT);
            try {
                prefs.flush();
                LOGGER.log(Level.FINE, String.format("%s and %s preferences saved in package %s", SLICE_SIZE, MAX_DRIFT, prefs.absolutePath()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, String.format("preferences cannot be saved: %s", e.getMessage()));
                LOGGER.log(Level.FINE, "Exception: ", e);
            }
        }
        LOGGER.log(Level.INFO, String.format("using TOTP parameter values:\n\t%s=%s,\n\t%s=%s", SLICE_SIZE, getTimeSliceSizeInSeconds(), MAX_DRIFT, getDriftUnits()));
    }
    
    /**
     * generates safe secret for TOTP
     *
     * @return string Base64 representation of secret
     */
    @Override
    public String generateSecret() {
        byte[] buffer = new byte[10];
        new SecureRandom().nextBytes(buffer);
        return new String(Base64.getEncoder().encode(buffer));
    }
    
    /**
     * returns timestamp valid for getTimeSliceSizeInSeconds() [s]
     *
     * @return current time slice
     */
    protected long getTime() {
        return System.currentTimeMillis() / 1000 / getTimeSliceSizeInSeconds();
    }
    
    /**
     * provides time based one time password of given secret (string representation) and current time
     *
     * @param secret TOTP secret
     * @return TOTP verification code
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    @Override
    public long getCurrentCode(String secret) throws NoSuchAlgorithmException, InvalidKeyException {
        return getCode(secret2bytes(secret), getTime());
    }
    
    /**
     * provides serial based one time password of given secret (string representation) and current OTP serial
     *
     * @param secret OTP secret
     * @param serial OTP serial (daily generated)
     * @return OTP verification code
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    @Override
    public long getCodeForSerial(String secret, int serial) throws NoSuchAlgorithmException, InvalidKeyException {
        return getCode(secret2bytes(secret), generateSerial(serial, 0));
    }
    
    /**
     * test variant for getCodeForSerial which takes also drift as a parameter
     *
     * @param secret OTP secret
     * @param serial OTP serial (daily generated)
     * @param drift  drift in time slice units
     * @return OTP verification code
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    protected long getCodeForSerialAndDrift(String secret, int serial, int drift) throws NoSuchAlgorithmException, InvalidKeyException {
        return getCode(secret2bytes(secret), generateSerial(serial, drift));
    }
    
    /**
     * provides time based code of given secret
     *
     * @param secret    bytes of secret
     * @param timePoint code generation time
     * @return numeric code
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    protected long getCode(byte[] secret, long timePoint) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec singKey = new SecretKeySpec(secret, "HmacSHA1");
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(timePoint);
        byte[] timeBytes = buffer.array();
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(singKey);
        byte[] hash = mac.doFinal(timeBytes);
        int offset = hash[19] & 0xf;
        long truncatedHash = hash[offset] & 0x7f;
        for (int i = 1; i < 4; i++) {
            truncatedHash <<= 8;
            truncatedHash |= hash[offset + i] & 0xff;
        }
        return truncatedHash % 1000000;
    }
    
    /**
     * converts Base64 encoded secret to byte array
     *
     * @param secret secret
     * @return byte array
     */
    protected byte[] secret2bytes(String secret) {
        return Base64.getDecoder().decode(secret.getBytes());
    }
    
    /**
     * verifies given code against reference
     *
     * @param secret secret (Base64 encoded)
     * @param code   given code
     * @return true if code is valid
     */
    @Override
    public boolean verify(String secret, long code) {
        long timePoint = getTime();
        long prevTimePoint = timePoint - getDriftUnits();
        long nextTimePoint = timePoint + getDriftUnits();
        try {
            for (long i = prevTimePoint; i <= nextTimePoint; i++) {
                if (code == getCode(secret2bytes(secret), i)) {
                    return true;
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            LOGGER.log(Level.SEVERE, String.format("something went wrong: %s", e.getMessage()));
            LOGGER.log(Level.FINE, "Exception: ", e);
        }
        return false;
    }
    
    /**
     * verifies given code against reference strict checking for yearly serial
     *
     * @param secret secret (Base64 encoded)
     * @param code   given code
     * @param serial daily serial
     * @return true if code is valid
     */
    @Override
    public boolean verify(String secret, long code, int serial) {
        try {
            for (int i = -getDriftUnits(); i <= getDriftUnits(); i++) {
                long calculated = generateSerial(serial, i);
                if (code == getCode(secret2bytes(secret), calculated)) {
                    return true;
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            LOGGER.log(Level.SEVERE, String.format("something went wrong with OTP verification: %s", e.getMessage()));
            LOGGER.log(Level.FINE, "Exception: ", e);
        }
        return false;
    }
    
    /**
     * Reads time slice size from user preference
     *
     * @return slice size in seconds
     */
    protected int getTimeSliceSizeInSeconds() {
        return prefs.getInt(SLICE_SIZE, DEFAULT_SLICE_SIZE);
    }
    
    /**
     * Reads max drift from user preference
     *
     * @return max drift size in time slices
     */
    protected int getDriftUnits() {
        return prefs.getInt(MAX_DRIFT, DEFAULT_MAX_DRIFT);
    }
    
    protected long generateSerial(int dailySerial, int drift) {
        long sliceSerial = getMODSliceSerial(drift);
        return generateTimeExpiringSerial(dailySerial, sliceSerial);
    }
    
    private long generateTimeExpiringSerial(int dailySerial, long currentSlice) {
        return dailySerial * 1000000 +
                currentSlice * 1000 +
                LocalDate.now().getDayOfYear();
    }
    
    /**
     * generates slice serial with drift accounting
     *
     * @param drift drift in slice units
     * @return time expiring (SLICE_SIZE seconds) serial
     */
    protected long getMODSliceSerial(int drift) {
        return (long) (LocalDateTime.now().get(ChronoField.MINUTE_OF_DAY) /
                ((getTimeSliceSizeInSeconds() * 1.0) / 60)) + drift;
    }
}
