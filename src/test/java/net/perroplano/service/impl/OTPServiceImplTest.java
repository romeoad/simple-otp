package net.perroplano.service.impl;

import net.perroplano.service.OTPService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static net.perroplano.service.impl.OTPServiceImpl.DEFAULT_MAX_DRIFT;
import static net.perroplano.service.impl.OTPServiceImpl.DEFAULT_SLICE_SIZE;
import static net.perroplano.service.impl.OTPServiceImpl.MAX_DRIFT;
import static net.perroplano.service.impl.OTPServiceImpl.SLICE_SIZE;

/**
 * TOTPServiceImplTest
 * <p>
 * Created by Krzysztof Osiecki <krzysztof.osiecki@icloud.com> on 2015-11-27.
 */
public class OTPServiceImplTest {

    OTPServiceImpl svc;
    String secret;

    @Before
    public void init() {
        svc = new OTPServiceImpl();
        svc.setupPreferences();
        secret = svc.generateSecret();
    }

    @Test
    public void testSimpleTOTP() throws NoSuchAlgorithmException, InvalidKeyException {
        long code = svc.getCurrentCode(secret);
        assertTrue("code from current slice should be valid", svc.verify(secret, code));
    }

    @Test
    public void testPreviousSlice() throws NoSuchAlgorithmException, InvalidKeyException, InterruptedException {
        long timeSlice = svc.getTime() - svc.getDriftUnits();
        long code = svc.getCode(svc.secret2bytes(secret), timeSlice);
        assertTrue("code from previous slice should be valid", svc.verify(secret, code));
    }

    @Test
    public void testNextSlice() throws NoSuchAlgorithmException, InvalidKeyException, InterruptedException {
        long nextSlice = svc.getTime() + svc.getDriftUnits();
        long code = svc.getCode(svc.secret2bytes(secret), nextSlice);
        assertTrue("code from next slice should be valid", svc.verify(secret, code));
    }

    @Test
    public void testExpirations() throws NoSuchAlgorithmException, InvalidKeyException, InterruptedException {
        long testSlice = svc.getTime() - svc.getDriftUnits() - 1; // pre-previous
        long code = svc.getCode(svc.secret2bytes(secret), testSlice);
        assertFalse("code for pre-previous slice should be invalid", svc.verify(secret, code));
        testSlice = svc.getTime() + svc.getDriftUnits() + 1; // next-to-next
        code = svc.getCode(svc.secret2bytes(secret), testSlice);
        assertFalse("code for next-to-next slice should be invalid", svc.verify(secret, code));
    }

    @Test
    public void testAbnormalSliceSizes() {
        Preferences prefs = Preferences.userNodeForPackage(OTPService.class);
        prefs.putInt(SLICE_SIZE, 13);
        prefs.putInt(MAX_DRIFT, 0);
        try {
            prefs.flush();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }
        svc.setupPreferences();
        assertEquals(prefs.getInt(SLICE_SIZE, 0), DEFAULT_SLICE_SIZE);
        assertEquals(prefs.getInt(MAX_DRIFT, 0), DEFAULT_MAX_DRIFT);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testNoAlgoithmException() throws NoSuchAlgorithmException, InvalidKeyException {
        svc = Mockito.mock(OTPServiceImpl.class);
        Mockito.doThrow(NoSuchAlgorithmException.class).when(svc).getCurrentCode(Mockito.anyString());
        try {
            long code = svc.getCurrentCode(secret);
            fail("should throw NoSuchAlgorithmException");
        } catch (NoSuchAlgorithmException nsae) {
            assertNotNull(nsae);
        }
    }

    // serial based instead of time based methods

    @Test
    public void testSerialBasedGeneration() throws NoSuchAlgorithmException, InvalidKeyException {
        long code0 = svc.getCodeForSerial(secret, 0);
        assertTrue(svc.verify(secret, code0, 0));
        long code1 = svc.getCodeForSerial(secret, 1);
        assertTrue(svc.verify(secret, code1, 1));
        assertTrue(code0 != code1);
    }

    @Test
    public void testTimeExpiringSerialBasedGeneration() throws NoSuchAlgorithmException, InvalidKeyException {
        long code = svc.getCodeForSerialAndDrift(secret, 1, -2);
        assertFalse("previous to previous slice code should be invalid", svc.verify(secret, code, 1));
        code = svc.getCodeForSerialAndDrift(secret, 1, 0);
        assertTrue("code for current slice should be valid", svc.verify(secret, code, 1));
        code = svc.getCodeForSerialAndDrift(secret, 1, 2);
        assertFalse("next to next slice code should be invalid", svc.verify(secret, code, 1));
    }
}
