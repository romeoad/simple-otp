# One Time Password Service

Simple and production ready TOTP and TSOTP implementation.

## Preparation
1. generate ```secret``` for each user and store it safely
1. reset ```serial``` for each user to 1 - if using TSOTP

## Time based One Time Password
1. generate OTP using given ```secret```, send it with SMS/mail/etc
1. verify given ```code``` against ```serial```

## Time and Serial based One Time Password
1. update ```serial``` with next value
1. generate OTP using given ```secret``` and ```serial```, send it with SMS/mail/etc
1. verify given ```code``` against ```secret``` and ```serial```
1. upon positive verification update ```serial``` with next value to prevent it from second time positive validation

and that's it!

Typical OTP message looks like:
```
Your validation code #1 on 2018-07-20 is:
123456
```

Enjoy!

K+